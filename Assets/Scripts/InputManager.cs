﻿using UnityEngine;

public class InputManager : MonoBehaviour
{
    private void FixedUpdate()
    {
        if (!GameController.Instance.IsOver)
        {
            if (!GameController.Instance.player1.GetComponent<AI>().enabled)
                GameController.Instance.player1.yAxisInput = Input.GetAxis("Player1");
            if (!GameController.Instance.player2.GetComponent<AI>().enabled)
                GameController.Instance.player2.yAxisInput = Input.GetAxis("Player2");
        }
        else
        {
            GameController.Instance.player1.yAxisInput = 0.0f;
            GameController.Instance.player2.yAxisInput = 0.0f;
        }
    }

    private void Update()
    {
        if (Input.GetButtonDown("Pause"))
            if(!GameController.Instance.IsOver)
                GameController.Instance.Pause();
    }
}
