﻿using UnityEngine;

public class GameSettings : MonoBehaviour
{
    public static GameSettings Instance { get; private set; }
    public bool multiplayer = false;
    public int scoreToWin = 3;
    public float delayBetweenRounds = 1.0f;
    public AIDifficulty aiDifficulty = AIDifficulty.EASY;
    public bool sounds = true;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        DontDestroyOnLoad(this);
    }
}
