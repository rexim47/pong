﻿using UnityEngine.SceneManagement;

public class GameoverMenu : Menu
{
    protected override void Start()
    {
        base.Start();
        GameController.Instance.gameOverEvent += ShowMenu;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        GameController.Instance.gameOverEvent -= ShowMenu;
    }

    protected override void AddListeners()
    {
        buttons[0].onClick.AddListener(OnPlayAgainButtonClick);
        buttons[1].onClick.AddListener(OnMainMenuButtonClick);
    }

    protected override void RemoveListeners()
    {
        buttons[0].onClick.RemoveListener(OnPlayAgainButtonClick);
        buttons[1].onClick.RemoveListener(OnMainMenuButtonClick);
    }

    private void OnPlayAgainButtonClick()
    {
        SceneManager.LoadScene("Main");
    }

    private void OnMainMenuButtonClick()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
