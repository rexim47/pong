﻿using UnityEngine.SceneManagement;

public class PauseMenu : Menu
{
    protected override void Start()
    {
        base.Start();
        GameController.Instance.gamePausedEvent += ShowMenu;
        GameController.Instance.gameUnpausedEvent += HideMenu;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        GameController.Instance.gamePausedEvent -= ShowMenu;
        GameController.Instance.gameUnpausedEvent -= HideMenu;
    }

    protected override void AddListeners()
    {
        buttons[0].onClick.AddListener(OnBackButtonClick);
        buttons[1].onClick.AddListener(OnRestartButtonClick);
        buttons[2].onClick.AddListener(OnMainMenuButtonClick);
    }

    protected override void RemoveListeners()
    {
        buttons[0].onClick.RemoveListener(OnBackButtonClick);
        buttons[1].onClick.RemoveListener(OnRestartButtonClick);
        buttons[2].onClick.RemoveListener(OnMainMenuButtonClick);
    }

    private void OnBackButtonClick()
    {
        GameController.Instance.Pause();
    }

    private void OnRestartButtonClick()
    {
        GameController.Instance.Pause();
        SceneManager.LoadScene("Main");
    }

    private void OnMainMenuButtonClick()
    {
        GameController.Instance.Pause();
        SceneManager.LoadScene("MainMenu");
    }
}
