﻿using UnityEngine;
using UnityEngine.UI;

public abstract class Menu : MonoBehaviour
{
    public string[] options;
    public Button[] buttons;

    protected virtual void Start()
    {
        for(int i=0;i<buttons.Length;i++)
        {
            buttons[i].GetComponentInChildren<Text>().text = options[i];
        }
        AddListeners();
    }

    protected virtual void OnDestroy()
    {
        RemoveListeners();
    }

    protected abstract void AddListeners();
    protected abstract void RemoveListeners();

    protected virtual void ShowMenu()
    {
        foreach (Button b in buttons)
        {
            b.gameObject.SetActive(true);
        }
        buttons[0].Select();
    }

    protected virtual void HideMenu()
    {
        foreach (Button b in buttons)
        {
            b.gameObject.SetActive(false);
        }
    }
}
