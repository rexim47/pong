﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : Menu
{
    public Dropdown difficultyDropdown;
    public Toggle soundsToggle;

    protected override void AddListeners()
    {
        buttons[0].onClick.AddListener(OnSingleplayerButtonClick);
        buttons[1].onClick.AddListener(OnMultiplayerButtonClick);
        buttons[2].onClick.AddListener(OnExitButtonClick);
        difficultyDropdown.onValueChanged.AddListener(OnDifficultyDropdownValueChanged);
        soundsToggle.onValueChanged.AddListener(OnSoundsToggleValueChanged);
    }

    protected override void RemoveListeners()
    {
        buttons[0].onClick.RemoveListener(OnSingleplayerButtonClick);
        buttons[1].onClick.RemoveListener(OnMultiplayerButtonClick);
        buttons[2].onClick.RemoveListener(OnExitButtonClick);
        difficultyDropdown.onValueChanged.RemoveListener(OnDifficultyDropdownValueChanged);
        soundsToggle.onValueChanged.RemoveListener(OnSoundsToggleValueChanged);
    }

    private void OnSingleplayerButtonClick()
    {
        GameSettings.Instance.multiplayer = false;
        SceneManager.LoadScene("Main");
    }

    private void OnMultiplayerButtonClick()
    {
        GameSettings.Instance.multiplayer = true;
        SceneManager.LoadScene("Main");
    }

    private void OnExitButtonClick()
    {
        Application.Quit();
    }

    private void OnDifficultyDropdownValueChanged(int value)
    {
        GameSettings.Instance.aiDifficulty = (AIDifficulty)value;
    }

    private void OnSoundsToggleValueChanged(bool value)
    {
        GameSettings.Instance.sounds = value;
    }
}
