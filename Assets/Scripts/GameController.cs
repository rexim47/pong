﻿using System.Collections;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; }
    private bool isPaused;
    public bool IsOver { get; private set; }

    public Player player1;
    public Player player2;
    private AI player2ai;
    public Ball ball;

    public delegate void GameControllerEventDelegate();
    public event GameControllerEventDelegate gamePausedEvent;
    public event GameControllerEventDelegate gameUnpausedEvent;
    public event GameControllerEventDelegate gameOverEvent;

    private void Awake()
    {
        Instance = this;
        isPaused = false;
        IsOver = false;

        player2ai = player2.GetComponent<AI>();

        //Jeżeli gra na 2 osoby to wyłączamy komponent AI u drugiego gracza
        if (GameSettings.Instance.multiplayer)
            player2ai.enabled = false;
        else //dla gry z CPU ustawiamy poziom trudności
            player2ai.difficulty = GameSettings.Instance.aiDifficulty;

#if UNITY_EDITOR
        Application.runInBackground = true;
#endif
    }

    public void EndRound(Player whoScored)
    {
        whoScored.IncreaseScore();
        if (whoScored.Score < GameSettings.Instance.scoreToWin)
            StartCoroutine("PrepareNextRound"); //następna runda, z małym opóźnieniem
        else
        {
            IsOver = true;

            //Wyświetlenie informacji o wygranej
            if (whoScored == player1)
            {
                player1.playerScoreText.text = "WIN!";
                player2.playerScoreText.text = "";
            }
            else
            {
                player2.playerScoreText.text = "WIN!";
                player1.playerScoreText.text = "";
            }

            //Wyłączamy komponenty graczy
            player1.enabled = false;
            player2.enabled = false;

            //Wyłączamy komponent AI drugiego gracza
            player2ai.enabled = false;

            //Ustawiamy piłkę na środku
            ball.Reset(true);

            //Menu po meczu
            gameOverEvent();
        }
    }

    private IEnumerator PrepareNextRound()
    {
        //Opóźnienie przed zresetowaniem piłki
        yield return new WaitForSeconds(GameSettings.Instance.delayBetweenRounds);
        ball.Reset(false);
    }

    public void Pause()
    {
        if (!isPaused) //pauza
        {
            isPaused = true;
            Time.timeScale = 0.0f;
            gamePausedEvent();
        }
        else //odpauzowanie
        {
            isPaused = false;
            Time.timeScale = 1.0f;
            gameUnpausedEvent();
        }
    }
}
