﻿using UnityEngine;

public class ScoreTrigger : MonoBehaviour
{
    public Player player;

    private void OnTriggerEnter2D(Collider2D other)
    {
        Ball ball = other.GetComponent<Ball>();
        if (ball == null)
            return;
        GameController.Instance.EndRound(player);
    }
}
