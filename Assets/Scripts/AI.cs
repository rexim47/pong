﻿using UnityEngine;

public class AI : MonoBehaviour
{
    private Player player;
    public Ball ball;

    public AIDifficulty difficulty;
    //Czasy reakcji na poszczególnych poziomach trudnośći (w sekundach)
    [Range(0.01f, 1.0f)] public float easyReactionTime = 0.3f;
    [Range(0.01f, 1.0f)] public float mediumReactionTime = 0.275f;
    [Range(0.01f, 1.0f)] public float hardReactionTime = 0.260f;
    private float currentReactionTime;
    public float slowDistance = 1.0f; //odległość przy której AI zaczyna poruszać się wolniej

	private void Start()
    {
        player = GetComponent<Player>();
        switch(difficulty)
        {
            case AIDifficulty.EASY: currentReactionTime = easyReactionTime; break;
            case AIDifficulty.MEDIUM: currentReactionTime = mediumReactionTime; break;
            case AIDifficulty.HARD: currentReactionTime = hardReactionTime; break;
        }
        //Update AI wywoływany z opóźnieniem (czas reakcji)
        InvokeRepeating("AIUpdate", currentReactionTime, currentReactionTime);
	}

    private void AIUpdate()
    {
        //Aktualnie duży czas reakcji i mała odległość "spowalniania" powodują,
        //że kolejne sprawdzenia odległości pada od piłki w osi OY
        //wykonywane są za rzadko, przez co pad zaczyna oscylować
        //Przy dużej odległości "spowalniania" różnica w poziomach trudności jest znikoma
        float distance = Mathf.Abs(ball.transform.position.y - transform.position.y);
        if (distance > slowDistance)
        {
            if (ball.transform.position.y > transform.position.y)
                player.yAxisInput = 1.0f;
            else if (ball.transform.position.y < transform.position.y)
                player.yAxisInput = -1.0f;
            else
                player.yAxisInput = 0.0f;
        }
        else
        {
            if (ball.transform.position.y > transform.position.y)
                player.yAxisInput = distance / slowDistance;
            else if (ball.transform.position.y < transform.position.y)
                player.yAxisInput = -distance / slowDistance;
            else
                player.yAxisInput = 0.0f;
        }
    }
}
