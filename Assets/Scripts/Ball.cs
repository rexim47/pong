﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(AudioSource))]
public class Ball : MonoBehaviour
{
    public float maxSpeed = 5.0f;
    private Rigidbody2D rb;
    private AudioSource[] audioSources;

	private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        audioSources = GetComponents<AudioSource>();

        //Na początku piłka leci do drugiego gracza
        rb.velocity = -(Vector2.right + Vector2.up).normalized * maxSpeed;
    }

    public void Reset(bool gameOver)
    {
        //Resetujemy pozycję
        transform.position = Vector2.zero;

        if (gameOver)
            rb.velocity = Vector2.zero;
        else
            rb.velocity = new Vector2(-rb.velocity.x,-rb.velocity.y * Random.Range(0.1f,0.5f)).normalized * maxSpeed; //piłka leci do gracza, który zdobył punkt
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        //Kolizja z padem
        if(col.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            //Dzwiek przy kolizji z padem
            if(GameSettings.Instance.sounds)
                audioSources[0].Play();

            Vector2 contactNormal = col.contacts[0].normal;
            //Jeżeli piłeczka uderzyła w pionową krawędź pada
            if (Mathf.Abs(contactNormal.x) == 1.0f && contactNormal.y == 0.0f)
            {
                //Na podstawie odległości piłeczki od środka pada określamy kierunek prędkości po odbiciu
                float distFromPadCenterNormalized = (transform.position.y - col.transform.position.y) / (col.transform.localScale.y * 0.5f);
                Vector2 velocityDirectionAfterReflection = new Vector2(col.contacts[0].normal.x, distFromPadCenterNormalized).normalized;
                rb.velocity = velocityDirectionAfterReflection * maxSpeed;
            }
        }
        //Dzwiek przy kolizji z krawedziami planszy
        if(col.gameObject.layer == LayerMask.NameToLayer("Border"))
            if (GameSettings.Instance.sounds)
                audioSources[1].Play();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        //Dzwiek przy wejsciu w trigger zliczajacy wynik
        if (col.gameObject.layer == LayerMask.NameToLayer("ScoreTrigger"))
            if (GameSettings.Instance.sounds)
                audioSources[2].Play();
    }

    private void OnCollisionExit2D(Collision2D col)
    {
        //Piłka utrzymuje cały czas maksymalną prędkość
        rb.velocity = rb.velocity.normalized * maxSpeed;
    }
}
