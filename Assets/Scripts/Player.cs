﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour
{
    public int Score { get; private set; }
    public float yAxisInput;
    public float maxSpeed = 5.0f;
    public Text playerScoreText;

    private Rigidbody2D rb;

    private void Start()
    {
        Score = 0;
        yAxisInput = 0.0f;
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        rb.velocity = Vector2.up * yAxisInput * maxSpeed;
    }

    private void OnDisable()
    {
        //Pad zatrzymuje się po wyłączeniu
        rb.velocity = Vector2.zero;
    }

    public void IncreaseScore()
    {
        //Zwiększamy wynik i aktualizujemy go na UI
        Score++;
        playerScoreText.text = Score.ToString();
    }
}
